namespace Infrastructure
{
    public interface IGamesProvider
    {
        string[] GetGameNames();
        void ChooseGame(int n);
        void ChooseGame(string name);
        bool Guess(string bet, out string result);
        int Result { get; }
    }
}