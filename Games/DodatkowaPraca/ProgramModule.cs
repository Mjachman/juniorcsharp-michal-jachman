﻿using Ninject.Modules;
using Infrastructure;
using GameLibrary;

namespace DodatkowaPraca
{
    class ProgramModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IGamesProvider>().To<GamesProvider>();
            Bind<IScoreManager>().ToProvider(new ScoreManagerProvider());

            Bind<IGame>().To<GuessGame>();
            Bind<IGame>().To<MastermindGame>();
        }
    }
}
